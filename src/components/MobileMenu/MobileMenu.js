/* eslint-disable no-unused-vars */
import React from 'react';
import styled, { keyframes } from 'styled-components';

import { DialogOverlay, DialogContent } from '@reach/dialog';

import { QUERIES, WEIGHTS } from '../../constants';

import UnstyledButton from '../UnstyledButton';
import Icon from '../Icon';
import VisuallyHidden from '../VisuallyHidden';

const MobileMenu = ({ isOpen, onDismiss }) => {
  return (
    <Overlay isOpen={isOpen} onDismiss={onDismiss}>
      <Backdrop />
      <AnimContent>
      <Content aria-label="Menu">
        <CloseButton onClick={onDismiss}>
          <Icon id="close" />
          <VisuallyHidden>Dismiss menu</VisuallyHidden>
        </CloseButton>
        <Filler />
        <Nav>
          <NavLink
            href="/sale"
            style={{ '--order': 1 }}
          >
            Sale
          </NavLink>
          <NavLink
            href="/new"
            style={{ '--order': 2 }}
          >
            New&nbsp;Releases
          </NavLink>
          <NavLink
            href="/men"
            style={{ '--order': 3 }}
          >
            Men
          </NavLink>
          <NavLink
            href="/women"
            style={{ '--order': 4 }}
          >
            Women
          </NavLink>
          <NavLink
            href="/kids"
            style={{ '--order': 5 }}
          >
            Kids
          </NavLink>
          <NavLink
            href="/collections"
            style={{ '--order': 6 }}
          >
            Collections
          </NavLink>
        </Nav>
        <Footer>
          <SubLink
            href="/terms"
            style={{ '--order': 7 }}
          >
            Terms and Conditions
          </SubLink>
          <SubLink
            href="/privacy"
            style={{ '--order': 8 }}
          >
            Privacy Policy
          </SubLink>
          <SubLink
            href="/contact"
            style={{ '--order': 9 }}
          >
            Contact Us
          </SubLink>
        </Footer>
      </Content>
      </AnimContent>
    </Overlay>
  );
};

const fadeIn = keyframes`
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
`

const slideIn = keyframes`
  from {
    transform: translateX(100%);
  }
  to {
    transform: translateX(0);
  }
`

const swingIn = keyframes`
  from {
    transform: rotateY(0deg);
  }
  to {
    transform: rotateY(180deg);
  }
`

const pop = keyframes`
  0% {
    transform: scale(1);
  }
  50% {
    transform: scale(1.5);
  }
  100% {
    transform: scale(1);
  }
`

const Overlay = styled(DialogOverlay)`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  justify-content: flex-end;
`;

const Backdrop = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: var(--color-backdrop);
  will-change: transform;
  animation: ${fadeIn} 1000ms;
  transition-timing-function: var(--ease-out);
`

const AnimContent = styled.div`
  transform: translateX(100%);
`

const Content = styled(DialogContent)`
  position: absolute;
  background: white;
  width: 300px;
  height: 100%;
  padding: 24px 32px;
  display: flex;
  flex-direction: column;
  // animation: ${slideIn} 250ms var(--ease-out) backwards;
  animation: ${swingIn} 2000ms var(--ease-out) both;
  transform-origin: left;
  animation-delay: 250ms;

  >* {
     transform: rotateY(180deg);
  }
`;

const CloseButton = styled(UnstyledButton)`
  position: absolute;
  top: 10px;
  left: 0;
  padding: 16px;
  will-change: transform;
  animation: ${pop} 500ms ease-in-out;
  animation-delay: 1000ms;
`;

const Nav = styled.nav`
  display: flex;
  flex-direction: column;
  gap: 16px;
`;

const LinkAnim = styled.a`
  will-change: transform;
  animation: ${fadeIn} 1000ms var(--ease-out) backwards;
  animation-delay: calc(700ms + calc(var(--order) * 200ms));
`

const NavLink = styled(LinkAnim)`
  color: var(--color-gray-900);
  font-weight: ${WEIGHTS.medium};
  text-decoration: none;
  font-size: 1.125rem;
  text-transform: uppercase;

  &:first-of-type {
    color: var(--color-secondary);
  }
`;

const Filler = styled.div`
  flex: 1;
`;

const Footer = styled.footer`
  flex: 1;
  display: flex;
  flex-direction: column;
  gap: 14px;
  justify-content: flex-end;
`;

const SubLink = styled(LinkAnim)`
  color: var(--color-gray-700);
  font-size: 0.875rem;
  text-decoration: none;
`;

export default MobileMenu;
